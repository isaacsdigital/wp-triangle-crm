<?php

/**
 * Created by PhpStorm.
 * User: fred
 * Date: 6/5/16
 * Time: 1:55 PM
 */

namespace Triangle\Http;

class Input {

	public static function get($key, $default = null)
	{
		if(!empty($_REQUEST[$key])) {
			return $_REQUEST[$key];
		}

		return $default;
	}

	public static function all()
	{
		return $_REQUEST;
	}

	public static function has($key)
	{
		return (!empty($_REQUEST[$key]));
	}

	public static function hasAll(array $keys = [ ])
	{
		foreach($keys as $key) {
			if(!static::has($key)) {
				return false;
			}
		}

		return true;
	}

	public static function only(array $keys = [ ])
	{
		$results = [ ];

		foreach($keys as $key) {
			if(static::has($key)) {
				$results[] = static::get($key);
			}
		}

		return $results;
	}
}