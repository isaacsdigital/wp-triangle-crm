<?php
/**
 * Created by PhpStorm.
 * User: fred
 * Date: 6/6/16
 * Time: 12:31 PM
 */

namespace Triangle\Validators;


use Triangle\Plugin;

class BlockBin {

	public function __invoke($field, $input, $param = null)
	{
		$filePath  = Plugin::instance()->getPluginPath() . "assets/blockedBins.csv";
		$contents  = file_get_contents($filePath);
		$lines     = str_getcsv($contents);
		$first_six = substr($input["creditCard"], 0, 6);
		$last_four = substr($input["creditCard"], 12, 4);

		if(in_array($first_six, $lines)) {
			error_log("bin-block occurred for bin: " . $first_six . "***" . $last_four);
			return false;
		}

		return true;
	}
}