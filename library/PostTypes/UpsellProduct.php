<?php
/**
 * Created by PhpStorm.
 * User: fred
 * Date: 7/4/16
 * Time: 7:17 PM
 */

namespace Triangle\PostTypes;


use Triangle\Vendor\CPT;

class UpsellProduct extends CPT {
	
	public function __construct()
	{
		parent::__construct([
			                    'post_type_name' => 'triangle_product',
			                    'singular'       => 'Upsell Product',
			                    'plural'         => 'Upsell Products'
		                    ], [
			                    'show_in_menu'        => "triangle-crm",
			                    'supports'            => [ "title" ],
			                    'publicly_queryable'  => false,
			                    'public'              => true,
			                    'show_in_nav_menus'   => false,
			                    'exclude_from_search' => true,
			                    'capabilities'        => [
				                    'create_posts' => false, // false < WP 4.5, credit @Ewout
				                    'delete_posts' => true
			                    ]
		                    ]);

		$this->columns([
			               'cb'         => '<input type="checkbox" />',
			               'title'      => __('Name'),
			               'offer_id'  => "Offer ID",
			               'project_id' => __('Project ID'),
			               'product_id' => __("Product ID"),
			               'default_price'  => __("Default Price"),
			               'date'       => __('Date')
		               ]);

		$this->populate_column('offer_id', function($column, $post) {
			echo $post->ID;
		});
		$this->populate_column('project_id', function($column, $post) {
			echo get_field('project_id', $post->ID);
		});
		$this->populate_column('product_id', function($column, $post) {
			echo get_field('product_id', $post->ID);
		});
		$this->populate_column('plan_id', function($column, $post) {
			echo get_field('plan_id', $post->ID);
		});

		$this->populate_column('default_price', function($column, $post) {
			$price = get_field('default_price', $post->ID);
			echo  "$" . number_format($price, 2);
		});
	}
	
	
}