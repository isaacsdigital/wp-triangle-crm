<?php
/**
 * Created by PhpStorm.
 * User: fred
 * Date: 5/31/16
 * Time: 5:01 PM
 */

namespace Triangle\PostTypes;

use Triangle\Vendor\CPT;

class Subscription extends CPT {
	
	public function __construct()
	{
		parent::__construct([
			                    'post_type_name' => 'triangle_subs',
			                    'singular'       => 'Subscription Plan',
			                    'plural'         => 'Subscription Plans'
		                    ], [
			                    'show_in_menu'        => "triangle-crm",
			                    'supports'            => [ "title" ],
			                    'publicly_queryable'  => false,
			                    'public'              => true,
			                    'show_in_nav_menus'   => false,
			                    'exclude_from_search' => true,
			                    'capabilities'        => [
				                    'create_posts' => false, // false < WP 4.5, credit @Ewout
				                    'delete_posts' => true
			                    ]
		                    ]);

		$this->columns([
			               'cb'         => '<input type="checkbox" />',
			               'title'      => __('Name'),
			               'offer_id'   => __("Offer ID"),
			               'project_id' => __('Project ID'),
			               'plan_id'    => __("Plan ID"),
			               'date'       => __('Date')
		               ]);

		$this->populate_column('offer_id', function($column, $post) {
			echo $post->ID;
		});

		$this->populate_column('project_id', function($column, $post) {
			echo get_field('project_id', $post->ID); // ACF get_field() function
		});

		$this->populate_column('plan_id', function($column, $post) {
			echo get_field('plan_id', $post->ID);
		});


	}


}