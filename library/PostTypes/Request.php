<?php
/**
 * Created by PhpStorm.
 * User: fred
 * Date: 5/31/16
 * Time: 4:59 PM
 */

namespace Triangle\PostTypes;

use Triangle\Vendor\CPT;

class Request extends CPT {

	public function __construct()
	{
		parent::__construct([
			                    'post_type_name' => 'triangle_request',
			                    'singular'       => 'Request',
			                    'plural'         => 'Request Log',
			                    'slug'           => 'request',
		                    ], [
			                    'show_in_menu'        => "triangle-crm",
			                    'publicly_queryable'  => false,
			                    'public'              => true,
			                    'show_in_nav_menus'   => false,
			                    'exclude_from_search' => true,
			                    'supports'            => [ 'title' ],
			                    'capabilities'        => [
				                    'create_posts' => false, // false < WP 4.5, credit @Ewout
				                    'delete_posts' => true
			                    ]
		                    ]);

		add_action('load-post.php', [ $this, 'register_meta_boxes' ]);
		add_action('load-post-new.php', [ $this, 'register_meta_boxes' ]);
	}

	public function register_meta_boxes()
	{
		add_meta_box('triangle-prospect-request', 'Request', [ $this, 'render_request_metabox' ], "triangle_request");
		add_meta_box('triangle-prospect-response', 'Request', [ $this, 'render_response_metabox' ], "triangle_request");
	}

	public function render_request_metabox($post)
	{

		echo "Request Data";
		printf("<pre>%s</pre>", json_encode(get_post_meta($post->ID, "request"), JSON_PRETTY_PRINT));
	}

	public function render_response_metabox($post)
	{
		echo "Response Data";
		printf("<pre>%s</pre>", json_encode(get_post_meta($post->ID, "response"), JSON_PRETTY_PRINT));
	}

	/**
	 * Save soap request and response for logging purposes
	 * @param $request
	 * @param $response
	 */
	public static function save($request, $response)
	{
		
		$post_id = wp_insert_post([
			                          "post_title" => sprintf("%s %s", $request["firstName"], $request["lastName"]),
			                          "post_type"  => "triangle_request",
			                          'status'     => "publish"
		                          ]);
		
		update_post_meta($post_id, "request", $request);
		update_post_meta($post_id, "response", $response);

		do_action('wp_triangle_request_save', [
			"request" => $request,
			"response" => $response
		]);
	}
}