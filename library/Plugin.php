<?php
/**
 * Created by PhpStorm.
 * User: fred
 * Date: 5/26/16
 * Time: 5:03 PM
 */

namespace Triangle;


use Triangle\API\TriangleAjax;
use Triangle\API\TriangleAPI;
use Triangle\PostTypes\Request;
use Triangle\PostTypes\UpsellProduct;
use Triangle\Vendor\CPT;
use Triangle\PostTypes\Subscription;

class Plugin {

	private static $instance = null;

	public $plugin_url;
	public $plugin_path;

	private function __construct()
	{
		$this->plugin_path = TRIANGLE_PLUGIN_PATH;
		$this->plugin_url  = TRIANGLE_PLUGIN_URL;


		add_action('plugins_loaded', [$this, 'start_session'], 1);

//		load_plugin_textdomain($this->text_domain, false, $this->plugin_path . '\lang');
		register_activation_hook(__FILE__, [ $this, 'activation' ]);
		register_deactivation_hook(__FILE__, [ $this, 'deactivation' ]);


		$this->register_option_pages();
		$this->register_post_types();
		$this->register_custom_fields();
		$this->register_ajax_endpoints();
		add_shortcode('fireAffiliatePixel', [$this, 'fireAffiliatePixel']);
	}

	private function save_to_session($key) {
		if(!empty($_REQUEST[$key])) {
			$_SESSION[$key] = $_REQUEST[$key];
		}
	}
	
	public function start_session() {
		if(!session_id()) {
			session_start();
		}
		
		$this->save_to_session("AFFID");
		$this->save_to_session("SID");
		$this->save_to_session("CID");
		$this->save_to_session("clickid");
	}

	/**
	 * @return Plugin
	 */
	public static function instance()
	{
		if(is_null(static::$instance)) {
			static::$instance = new static();
		}

		return static::$instance;
	}

	/**
	 * Register Custom Post Types
	 */
	protected function register_post_types()
	{
		new UpsellProduct();
		new Subscription();
		new Request();
	}

	/**
	 * Register Admin Option Pages
	 */
	protected function register_option_pages()
	{
		if(!function_exists('acf_add_options_page')) {
			throw new \Exception('Advanced Custom Fields is not installed or activated');
		}

		acf_add_options_page([
			                     'page_title' => 'TriangleCRM',
			                     'menu_title' => 'TriangleCRM',
			                     'menu_slug'  => 'triangle-crm',
			                     'capability' => 'manage_options',
			                     'icon_url'   => 'dashicons-cart',
			                     'position'   => '25',
			                     'redirect'   => false
		                     ]);

		acf_add_options_sub_page([
			                         'page_title' => 'TriangleCRM API Configuration',
			                         'menu_title' => 'Configuration',
			                         'menu_slug'  => 'triangle-crm-config',
			                         'capability' => 'manage_options',
			                         'parent'     => "triangle-crm",
			                         'redirect'   => false
		                         ]);


	}

	/**
	 * Register ACF custom fields from custom-fields directory
	 */
	protected function register_custom_fields()
	{
		// check to see if ACF is installed
		if(!function_exists("register_field_group")) {
			throw new \Exception('Advanced Custom Fields is not installed or activated');
		}

		// construct path to custom post types
		$path = sprintf("%s/custom-fields", $this->plugin_path);

		// require() every file in the directory
		foreach(new \DirectoryIterator($path) as $fileInfo) {
			// ignore dot directories
			if($fileInfo->isDot()) {
				continue;
			}
			// load file
			$config = require($fileInfo->getRealPath());
			register_field_group($config);
		}

	}

	/**
	 * Register Ajax EndPoints
	 */
	protected function register_ajax_endpoints()
	{
		(new TriangleAjax())->register();
	}

	/**
	 * Called on plugin activation
	 */
	protected function activation()
	{

	}

	protected function deactivation()
	{

	}

	/**
	 * @return mixed
	 */
	public function getTextDomain()
	{
		return $this->text_domain;
	}

	/**
	 * @param mixed $text_domain
	 */
	public function setTextDomain($text_domain)
	{
		$this->text_domain = $text_domain;
	}

	/**
	 * @return mixed
	 */
	public function getPluginUrl()
	{
		return $this->plugin_url;
	}

	/**
	 * @param mixed $plugin_url
	 */
	public function setPluginUrl($plugin_url)
	{
		$this->plugin_url = $plugin_url;
	}

	/**
	 * @return mixed
	 */
	public function getPluginPath()
	{
		return $this->plugin_path;
	}

	/**
	 * @param mixed $plugin_path
	 */
	public function setPluginPath($plugin_path)
	{
		$this->plugin_path = $plugin_path;
	}

	public function getConfig($key, $default = null)
	{
		$field = get_field($key, 'option');
		if(empty($field)) {
			return $default;
		}

		return $field;
	}

	public function fireAffiliatePixel($atts) {
		$a = shortcode_atts( array(
			                     'pageTypeId' => 5
		                     ), $atts );
		
		$triangle = new TriangleAPI();
		$response = $triangle->fireAffiliatePixel($a["pageTypeId"]);
	    return $response;
	}
}
