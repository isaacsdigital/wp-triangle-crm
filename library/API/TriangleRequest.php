<?php

namespace Triangle\API;

use Triangle\Plugin;
use Triangle\Validators\BlockBin;
use Triangle\Vendor\Validator;

/**
 * Created by PhpStorm.
 * User: fred
 * Date: 5/23/16
 * Time: 6:30 PM
 */
abstract class TriangleRequest {

	protected $data = [ ];

	protected $contactRules = [
		"firstName" => "required|alpha_space",
		"lastName"  => "required|alpha_space",
		"email"     => "required|valid_email"
	];

	protected $addressRules = [
		"address1" => "required|alpha_space",
		"address2" => "alpha_space",
		"city"     => "required|alpha_space",
		"state"    => "required|alpha_space|min_len,2",
		"zip"      => "required",
//		"country"  => "required|alpha_space",
		"phone"    => "required"
	];

	protected $paymentRules = [
		"paymentType" => "alpha_numeric",
		"creditCard"  => "required|valid_cc|block_bin",
		"cvv"         => "required|numeric|max_len,3|min_len,3",
		"expMonth"    => "required|numeric|min_numeric,1|max_numeric,12",
		"expYear"     => "required|numeric|min_len,2|max_len,4",
	];

	/**
	 * @var Validator
	 */
	protected $validator;

	public function __construct(array $data = [])
	{
		if(empty($data)) {
			$this->data = $_REQUEST;
		} else {
			$this->data = $data;
		}
		
		Validator::add_validator("block_bin", new BlockBin());

	}



	/**
	 * Validate current request
	 */
	public function validate($blockBins = false)
	{

		$this->validator = new Validator();
		$this->validator->validation_rules($this->rules());
		$this->validator->filter_rules($this->filters());

		// sanitize
		$inputs = $this->validator->sanitize($this->data);

		// validate
		$validation_result = $this->validator->run($inputs);

		if($validation_result === false) {
			throw new ValidationException($this->validator->errors());
		} else {
			$this->data = $validation_result; // validation successful
		}
	}



	/**
	 * @param $key
	 * @param null $default
	 * @return mixed|null
	 */
	public function get($key, $default = null)
	{
		if(!empty($this->data[$key])) {
			return $this->data[$key];
		}

		return $default;
	}

	/**
	 * @return mixed - validation rules
	 */
	public abstract function rules();

	/**
	 * @return mixed - filter rules
	 */
	public abstract function filters();

	/**
	 * Return an array of data to be sent to Triangle API as request
	 * @return array
	 */
	public abstract function getSoapRequestData();


	/**
	 * Populate authentication information in request from configuration
	 * @return array
	 */
	protected function populateAuth()
	{
		$plugin = Plugin::instance();

		return [
			"username" => $plugin->getConfig("api_user_name"),
			"password" => $plugin->getConfig("api_password")
		];
	}

	/**
	 * Populate basic contact information:
	 * First and Last Name and Email
	 * @return array
	 */
	protected function populateContact()
	{
		return [
			"firstName" => $this->get("firstName"),
			"lastName"  => $this->get("lastName"),
			"email"     => $this->get("email")
		];
	}


	/**
	 * Boilerplate method to populate addresss information from http / json request.
	 * If the information doesnt exist in the request, it will not be returned.
	 *
	 * @return array
	 */
	protected function populateAddress()
	{
		return [
			"address1" => $this->get("address1"),
			"address2" => $this->get("address2"),
			"city"     => $this->get("city"),
			"state"    => $this->get("state"),
			"zip"      => $this->get("zip"),
			"country"  => $this->get("country", "US"),
			"phone"    => $this->get("phone")
		];
	}

	/**
	 * Boilerplate method to populate payment information from http / json request
	 *
	 * @return array
	 */
	protected function populatePayment()
	{
		return [
			"paymentType" => intval($this->get("paymentType")),
			"creditCard"  => intval($this->get("creditCard")),
			"cvv"         => intval($this->get("cvv")),
			"expMonth"    => intval($this->get("expMonth")),
			"expYear"     => intval($this->get("expYear")),
		];
	}

	/**
	 * Triangle API rejects requests where the source ip is a local loopback.
	 * This function returns a spoofed but valid ip if the source is a loopback.
	 * @return array
	 */
	protected function populateIpAddress()
	{
		$data = [ ];

		$spoofedIPs = [
			"127.0.0.1",
			"127.0.1.1"
		];

		if(!empty($_SERVER['REMOTE_ADDR']) && !in_array($_SERVER["REMOTE_ADDR"], $spoofedIPs)) {
			$data["ip"] = $_SERVER['REMOTE_ADDR'];
		} else {
			$data["ip"] = "12.215.31.134";
		}

		return $data;
	}


}