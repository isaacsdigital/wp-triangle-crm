<?php
namespace Triangle\API;

use Triangle\Validators\BlockBin;
use Triangle\Vendor\Validator;

/**
 * Created by PhpStorm.
 * User: fred
 * Date: 5/23/16
 * Time: 6:34 PM
 */
class CreateSubscriptionRequest extends TriangleRequest {

	/**
	 * @return mixed
	 */
	public function getSoapRequestData()
	{
		$offer   = get_post($this->get("offerId"));
		$auth    = $this->populateAuth();
		$address = $this->populateAddress();
		$payment = $this->populatePayment();
		$name    = $this->populateContact();
		$ip      = $this->populateIpAddress();

		$subscription = [
			"prospectID"            => intval($this->get("prospectId")),
			"planID"                => intval(get_field('plan_id', $offer->ID)),
			"trialPackageID"        => intval(get_field('trial_package_id', $offer->ID)),
			"chargeForTrial"        => boolval(get_field('charge_for_trial', $offer->ID)),
			"sendConfirmationEmail" => boolval(get_field('send_confirmation_email', $offer->ID))
		];

		$data = array_merge($auth, $address, $payment, $payment, $name, $ip, $subscription);


		if(!empty($_SESSION["CID"])) {
			$data["campaignID"] = $_SESSION["CID"];
		}
		
		if(!empty($_SESSION["AFFID"]))
		{
			$data["affiliate"] = $_SESSION["AFFID"];
		}

		if(!empty($_SESSION["SID"]))
		{
			$data["subAffiliate"] = $_SESSION["SID"];
		}

		return $data;
	}


	public function rules()
	{
		$rules = [
			"offerId"    => "required|numeric",
			"prospectId" => "required|numeric"
		];

		return array_merge($this->contactRules, $this->addressRules, $this->paymentRules, $rules);
	}

	public function filters()
	{
		return [ ];
	}
}