<?php
/**
 * Created by PhpStorm.
 * User: fred
 * Date: 9/29/16
 * Time: 3:43 PM
 */

namespace Triangle\API;


class FireAffiliatePixelRequest extends TriangleRequest {
	
	/**
	 * @return mixed - validation rules
	 */
	public function rules() {
		return [
			"pageTypeId" => 'required|numeric'
		];
	}
	
	/**
	 * @return mixed - filter rules
	 */
	public function filters() {
		return [];
	}
	
	/**
	 * Return an array of data to be sent to Triangle API as request
	 * @return array
	 */
	public function getSoapRequestData() {
		
		$data = [
			"pageTypeID" => $this->get('pageTypeId')
		];
		
		$prospectId = $this->get('prospectId');
		if(!is_null($prospectId)) {
			$data["prospectID"] = $prospectId;
		}
		
		if(!empty($_SESSION["AFFID"])) {
			$data["affiliate"] = $_SESSION["AFFID"];
		}
		
		if(!empty($_SESSION["clickid"])) {
			$data["clickId"] = $_SESSION["clickid"];
		}
		
		$auth  = $this->populateAuth();
		
		
		return array_merge($data, $auth);
	}
	
	public function getHitsJS() {
		$pageTypeId = 	$this->get('pageTypeId');
		$affid = null;
		$sid = null;
		$cid = null;
		
		if(!empty($_SESSION["AFFID"])) {
			$affid = $_SESSION["AFFID"];
		}
		
		if(!empty($_SESSION["SID"])) {
			$sid = $_SESSION["SID"];
		}
		
		if(!empty($_SESSION["CID"])) {
			$cid = $_SESSION["CID"];
		}
		
		$fmt = "https://edge.trianglecrm.com/pixel/hit.js?aff=%s&sub=%s&pid=%s&cid=%s";
		return sprintf($fmt, $affid, $sid, $pageTypeId,$cid);
	}
}