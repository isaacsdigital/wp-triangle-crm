<?php
namespace Triangle\API;

/**
 * Created by PhpStorm.
 * User: fred
 * Date: 5/23/16
 * Time: 6:28 PM
 */
class CreateProspectRequest extends TriangleRequest {
	
	/**
	 * @return mixed
	 * @throws \InvalidArgumentException
	 */
	public function getSoapRequestData()
	{
		$offerId = $this->get('offerId', null);

		if(is_null($offerId))
		{
			throw new \InvalidArgumentException("You must supply an offer id");
		}

		$offer   = get_post($offerId);
		$auth    = $this->populateAuth();
		$address = $this->populateAddress();
		$contact = $this->populateContact();
		$ip      = $this->populateIpAddress();

		$prospect = [
			"productTypeID" => get_field('project_id', $offer->ID)
		];


		$data = array_merge($auth, $address, $contact, $ip, $prospect);

		if(!empty($_SESSION["CID"])) {
			$data["campaignID"] = $_SESSION["CID"];
		}

		if(!empty($_SESSION["AFFID"]))
		{
			$data["affiliate"] = $_SESSION["AFFID"];
		}

		if(!empty($_SESSION["SID"]))
		{
			$data["subAffiliate"] = $_SESSION["SID"];
		}

		
		return $data;
	}


	/**
	 * @return mixed - validation rules
	 */
	public function rules()
	{
		$rules = [
			"offerId" => "required|numeric"
		];

		return array_merge($this->contactRules, $this->addressRules, $rules);
	}

	/**
	 * @return mixed - filter rules
	 */
	public function filters()
	{
		return [ ];
	}
}