<?php
/**
 * Created by PhpStorm.
 * User: fred
 * Date: 6/21/16
 * Time: 4:44 PM
 */

namespace Triangle\API;


class ValidationException extends \RuntimeException {

	private $errors;
	
	// Redefine the exception so message isn't optional
	public function __construct(array $errors = [ ], \Exception $previous = null)
	{
		// some code
		$message      = "A Validation Error Occurred";
		$this->errors = $errors;
		// make sure everything is assigned properly
		parent::__construct($message, 0, $previous);
	}

	public function setErrors(array $errors = [ ])
	{
		$this->errors = $errors;
	}

	public function getErrors()
	{
		return $this->errors;
	}
}