<?php
namespace Triangle\API;

use Triangle\Plugin;
use Triangle\PostTypes\Prospect;
use Triangle\PostTypes\Request;
use Triangle\PostTypes\Subscription;

/**
 * Created by PhpStorm.
 * User: fred
 * Date: 5/23/16
 * Time: 6:41 PM
 */
class TriangleAPI {

	/**
	 * @var \SoapClient
	 */
	protected $client;

	/**
	 * TriangleApi constructor. Sets up an internal soapClient
	 */
	public function __construct()
	{
		$plugin       = Plugin::instance();
		$domain       = $plugin->getConfig("api_host_name");
		$endpoint     = sprintf("https://%s.trianglecrm.com/api/2.0/billing_ws.asmx?WSDL", $domain);
		$this->client = new \SoapClient($endpoint);
	}
	
	public function fireAffiliatePixel($data) {
		
		$response = $this->client->FireAffiliatePixel($data);
		
		return $this->handleResponse($response->FireAffiliatePixelResult);
		
	}

	/**
	 * @param $data
	 * @return mixed
	 */
	public function createProspect($data)
	{
		$data     = \apply_filters('wp_triangle_prospect_data', $data);
		$response = $this->client->createProspect($data);

		Request::save($data, $response);

		do_action('wp_triangle_create_prospect', [
			"request"  => $data,
			"response" => $response
		]);

		return $this->handleResponse($response->CreateProspectResult);
	}

	/**
	 * @param $data
	 * @return mixed
	 */
	public function createSubscription($data)
	{
		$data = \apply_filters('wp_triangle_subscription_data', $data);

		$response = $this->client->createSubscription($data);

		Request::save($data, $response);

		do_action('wp_triangle_create_subscription', [
			"request"  => $data,
			"response" => $response
		]);

		return $this->handleResponse($response->CreateSubscriptionResult);
	}

	public function chargeSalesExistingCustomer($data)
	{
		$data = \apply_filters("wp_triangle_charge_sales_existing_customer_data", $data);

		$response = $this->client->chargeSalesExistingCustomer($data);

		Request::save($data, $response);

		do_action("wp_triangle_charge_sales_existing_customer", [
			"request"  => $data,
			"response" => $response
		]);

		return $this->handleResponse($response->ChargeSalesExistingCustomerResult);

	}

	/**
	 * @param $responseResult
	 * @return mixed
	 */
	protected function handleResponse($responseResult)
	{
		if($responseResult->State === "Error") {
			throw new \RuntimeException($responseResult->ErrorMessage);
		}

		return $responseResult->ReturnValue;
	}
}