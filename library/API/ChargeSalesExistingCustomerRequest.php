<?php
/**
 * Created by PhpStorm.
 * User: fred
 * Date: 7/4/16
 * Time: 6:43 PM
 */

namespace Triangle\API;


class ChargeSalesExistingCustomerRequest extends TriangleRequest {
	
	/**
	 * @return mixed - validation rules
	 */
	public function rules()
	{
		// TODO: Implement rules() method.
		return [
			"offerId"    => "required|numeric",
			"prospectId" => "required|numeric"
		];
	}

	/**
	 * @return mixed - filter rules
	 */
	public function filters()
	{
		// TODO: Implement filters() method.
		return [ ];
	}

	/**
	 * Return an array of data to be sent to Triangle API as request
	 * @return array
	 */
	public function getSoapRequestData()
	{
		$offer = get_post($this->get("offerId"));
		$auth  = $this->populateAuth();
		$ip    = $this->populateIpAddress();

		$sale = [
			"prospectID"            => intval($this->get("prospectId")),
			"productTypeID"         => get_field('project_id', $offer->ID),
			"sendConfirmationEmail" => boolval(get_field('send_confirmation_email', $offer->ID)),
			"productList"           => [
				"ProductCodeItem" => [
					[
						"ProductID" => get_field('product_id', $offer->ID),
						"Amount"    => floatval($this->get('price')),
						"Quantity"  => 1,
						"Upsell"    => 1
					]
				]
			]
		];

		$data = array_merge($auth, $ip, $sale);


		if(!empty($_SESSION["CID"])) {
			$data["campaignID"] = $_SESSION["CID"];
		}

		if(!empty($_SESSION["AFFID"]))
		{
			$data["affiliate"] = $_SESSION["AFID"];
		}

		if(!empty($_SESSION["SID"]))
		{
			$data["subAffiliate"] = $_SESSION["SID"];
		}
		
		return $data;
	}
}