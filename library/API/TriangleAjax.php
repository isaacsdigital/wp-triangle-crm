<?php
namespace Triangle\API;

/**
 * Created by PhpStorm.
 * User: fred
 * Date: 5/23/16
 * Time: 6:14 PM
 */
class TriangleAjax {
	
	
	/**
	 * Register Ajax Endpoints
	 */
	public function register()
	{
		$this->connect("create_prospect", "createProspect");
		$this->connect("create_subscription", "createSubscription");
		$this->connect("charge_sales_existing_customer", "chargeSalesExistingCustomer");
		$this->connect('fire_affiliate_pixel', "fireAffiliatePixel");
	}
	
	protected function connect($action, $method)
	{
		$adminAction  = sprintf("wp_ajax_%s", $action);
		$publicAction = sprintf("wp_ajax_nopriv_%s", $action);
		$callback     = [ $this, $method ];
		
		add_action($adminAction, $callback, true);
		add_action($publicAction, $callback, true);
	}

	protected function sendValidationError($data)
	{
		header("HTTP/1.0 422 Validation Error");
		wp_send_json_error($data);
	}

	protected function sendApplicationError($data) {
		header("HTTP/1.0 400 Application Error");
		wp_send_json_error($data);
	}
	
	/**
	 * API endpoint to create a prospect in triangle crm
	 */
	public function createProspect()
	{
		try {
			$api     = new TriangleAPI();
			$request = new CreateProspectRequest();
			$request->validate();
			$prospect = $request->getSoapRequestData();
			$result   = $api->createProspect($prospect);
			wp_send_json_success($result);
		} catch(ValidationException $ex) {
			$this->sendValidationError([
				             "message" => $ex->getMessage(),
				             "errors"  => $ex->getErrors()
			             ]);
		} catch(\Exception $ex) {
			$this->sendApplicationError([ "message" => $ex->getMessage() ]);
		}
		
	}
	
	
	/**
	 * API endpoint to create a subscription in triangle crm
	 */
	public function createSubscription()
	{
		try {
			$api     = new TriangleAPI();
			$request = new CreateSubscriptionRequest();
			$request->validate();
			$subscription = $request->getSoapRequestData();
			$result       = $api->createSubscription($subscription);
			wp_send_json_success($result);
		} catch(ValidationException $ex) {
			$this->sendValidationError([
				             "message" => $ex->getMessage(),
				             "errors"  => $ex->getErrors()
			             ]);
		} catch(\Exception $ex) {
			parse_str($ex->getMessage(), $response);
			$this->sendApplicationError($response);
		}
	}

	/**
	 * API endpoint to charge a sale to an existing customer
	 */
	public function chargeSalesExistingCustomer()
	{

		try {
			$api     = new TriangleAPI();
			$request = new ChargeSalesExistingCustomerRequest();
			$request->validate();
			$subscription = $request->getSoapRequestData();
			$result       = $api->chargeSalesExistingCustomer($subscription);
			wp_send_json_success($result);
		} catch(ValidationException $ex) {
			$this->sendValidationError([
				             "message" => $ex->getMessage(),
				             "errors"  => $ex->getErrors()
			             ]);
		} catch(\Exception $ex) {
			parse_str($ex->getMessage(), $response);
			$this->sendApplicationError($response);
		}
	}
	
	public function fireAffiliatePixel() {
		try{
			$api     = new TriangleAPI();
			$request = new FireAffiliatePixelRequest();
			$request->validate();
			$data = $request->getSoapRequestData();
			$result       = $api->fireAffiliatePixel($data);
			
			$hits = $request->getHitsJS();
			
			$payload = [
				"hits" => $hits,
			    "affiliatePixel" => $result
			];
			
			wp_send_json_success($payload);
		} catch(ValidationException $ex) {
			$this->sendValidationError([
				                           "message" => $ex->getMessage(),
				                           "errors"  => $ex->getErrors()
			                           ]);
		} catch(\Exception $ex) {
			return $this->sendApplicationError($ex->getMessage());
		}
		
		exit;
	}
	
}