<?php
/**
 * Plugin Name: Triangle CRM
 * Version: 0.1-alpha
 * Description: PLUGIN DESCRIPTION HERE
 * Author: YOUR NAME HERE
 * Author URI: YOUR SITE HERE
 * Plugin URI: PLUGIN SITE HERE
 * Text Domain: triangle-crm
 * Domain Path: /languages
 * @package Triangle CRM
 */
ini_set("soap.wsdl_cache_enabled", 0);

define("TRIANGLE_PLUGIN_PATH", plugin_dir_path(__FILE__));
define("TRIANGLE_PLUGIN_URL", plugin_dir_url(__FILE__));

spl_autoload_register(function ($class) {

	// project-specific namespace prefix
	$prefix = 'Triangle\\';

	// base directory for the namespace prefix
	$base_dir = __DIR__ . '/library/';

	// does the class use the namespace prefix?
	$len = strlen($prefix);
	if (strncmp($prefix, $class, $len) !== 0) {
		// no, move to the next registered autoloader
		return;
	}

	// get the relative class name
	$relative_class = substr($class, $len);

	// replace the namespace prefix with the base directory, replace namespace
	// separators with directory separators in the relative class name, append
	// with .php
	$file = $base_dir . str_replace('\\', '/', $relative_class) . '.php';

	// if the file exists, require it
	if (file_exists($file)) {
		require $file;
	}
});


\Triangle\Plugin::instance();
