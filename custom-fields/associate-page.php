<?php
/**
 * Created by PhpStorm.
 * User: fred
 * Date: 5/26/16
 * Time: 5:08 PM
 */
return [
	'id'         => 'acf_offer-workflows',
	'title'      => 'Offer Workflows',
	'fields'     => [
		[
			'key'           => 'field_57477f232af4e',
			'label'         => 'Associate this page with an Offer workflow?',
			'name'          => 'has_offer',
			'type'          => 'true_false',
			'message'       => '',
			'default_value' => 0,
		],
		[
			'key'               => 'field_57477f482af4f',
			'label'             => 'Offer Workflow',
			'name'              => 'offer',
			'type'              => 'post_object',
			'conditional_logic' => [
				'status'   => 1,
				'rules'    => [
					[
						'field'    => 'field_57477f232af4e',
						'operator' => '==',
						'value'    => '1',
					],
				],
				'allorany' => 'all',
			],
			'post_type'         => [
				0 => 'offer',
			],
			'taxonomy'          => [
				0 => 'all',
			],
			'allow_null'        => 0,
			'multiple'          => 0,
		],
	],
	'location'   => [
		[
			[
				'param'    => 'post_type',
				'operator' => '==',
				'value'    => 'page',
				'order_no' => 0,
				'group_no' => 0,
			],
		],
	],
	'options'    => [
		'position'       => 'side',
		'layout'         => 'default',
		'hide_on_screen' => [
		],
	],
	'menu_order' => 0,
];