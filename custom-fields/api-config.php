<?php
/**
 * Created by PhpStorm.
 * User: fred
 * Date: 5/27/16
 * Time: 4:08 PM
 */
return [
	'key' => 'group_5748d17d7e840',
	'title' => 'TriangleCRM API Configuration',
	'fields' => array (
		array (
			'key' => 'field_5748d19d89d3c',
			'label' => 'API User Name',
			'name' => 'api_user_name',
			'type' => 'text',
			'instructions' => 'Your TriangleCRM API user name. ',
			'required' => 1,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
			'readonly' => 0,
			'disabled' => 0,
		),
		array (
			'key' => 'field_5748d1f489d3d',
			'label' => 'API Password',
			'name' => 'api_password',
			'type' => 'text',
			'instructions' => 'Your TriangleCRM API password.',
			'required' => 1,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
			'readonly' => 0,
			'disabled' => 0,
		),
		array (
			'key' => 'field_5748d20c89d3e',
			'label' => 'API Host Name',
			'name' => 'api_host_name',
			'type' => 'text',
			'instructions' => '',
			'required' => 1,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '.trianglecrm.com',
			'maxlength' => '',
			'readonly' => 0,
			'disabled' => 0,
		),
	),
	'location' => array (
		array (
			array (
				'param' => 'options_page',
				'operator' => '==',
				'value' => 'triangle-crm-config',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'acf_after_title',
	'style' => 'default',
	'label_placement' => 'left',
	'instruction_placement' => 'field',
	'hide_on_screen' => '',
	'active' => 1,
	'description' => '',
];